import React, { useState, useEffect, } from 'react';
import instanciaAxios from './ajax/instanciaAxios';
import moment from 'moment';
import './ListaTarefas.css';


export default () => {

  
    const [laboratorios, setLaboratorios] = useState([]);
    const [tarefas, setTarefas] = useState([]);
    const [solicitacao, setSolicitacao] = useState([]);
    const [turno, setTurno] = useState([]);
    const [novaSolicitacao, setNovaSolicitacao] = useState("");
    const [novaTarefa, setNovaTarefa] = useState("");
    const [novoLaboratorio, setNovoLaboratorio] = useState("");
    const [novoTurno, setNovoTurno] = useState("");
    const [novaData, setNovaData] = useState("");
    const [novoAlerta, setNovoAlerta] = useState('off');
    

    useEffect(() => {
      pegarTarefas();
      pegarLaboratorios();
      pegarSolicitacao();
      pegarTurno();
          }, [])
    

    const pegarTarefas = async () => {
      try{
        const respTarefas = await instanciaAxios.get('../json/tarefas.json');
        setTarefas(respTarefas.data.Tarefas);
      }catch(e){
        console.log("get tarefas com defeito" + e.message);
      }
    };

    const pegarLaboratorios = async () => {
      try{
        const resposta = await instanciaAxios.get('../json/laboratorios.json');
        setLaboratorios(resposta.data.Laboratorios);
      }catch(e){
        console.log("get laboratorios com defeito" + e.message);
      }
    }
    
    const pegarSolicitacao = async () => {
      try {
        const respSolicitacao = await instanciaAxios.get('../json/solicitacao.json');
        setSolicitacao(respSolicitacao.data.Solicitacao);
      } catch(error){
        console.log("get solicitacao com defeito" + error.message);
      }
    }

    const pegarTurno = async () => {
      try{
        const respTurno = await instanciaAxios.get('../json/turno.json');
        setTurno(respTurno.data.Turno);
      } catch (err){
        console.log("get semana com defeito" + err.message);
      }
    }

   
   
    const OpcoesLaboratoriosComponente = () => {
     
        const laboratoriosJSX = laboratorios.map( ( item ) => {
          return(
            <option key={item.id} value={item.id}>
              {item.rotulo}
            </option>
          )
        })
      return laboratoriosJSX;
    };

    const OpcoesSolicitacaoComponente = () => {
      const solicitacaoJSX = solicitacao.map( (item) => {
        return(
        <option key={item.id} value={item.id}>
          {item.rotulo}
          </option>
        )
      }
      )
      return solicitacaoJSX;
    };
    

    const OpcoesTurnoComponente = () => {
      const turnoJSX = turno.map( (item) => {
        return(
          <div>
            <input className="input-radio" type="radio" 
              id={item.id}  
              value={item.id} 
              name="turno" 
              onChange={ (evento) => setNovoTurno (evento.target.value)}
              checked={item.id === novoTurno} />
             <label htmlFor={item.id} > {item.rotulo}</label> 
          </div>
        )
      }
      )
      return turnoJSX;
    };


    
      const AlertaIconeComponente = () => {
        return (
          
          <img id='alert-icon' src='/img/alert.png' />
          
          )
      }

      const CorpoTabelaComponente = () => {

        return (
          <tbody>
            { tarefas.map( (item) => {
               return(

          <LinhaTabelaComponente 
            key={item.id}
            value={item.id}
            idItem={item.id}
            propsTarefas={item.tarefas} 
            propsLab={item.idLaboratorio}
            propsSolicitacao={item.idSolicitacao}
            propsTurno={item.idTurno}
            propsAlerta={item.urgente}
            propsDataSolic={item.dataSolic}
            propsDataFinal={item.dataFinal}
                      />
                  )
            } )}
          </tbody>
                
          )
      };

      const LinhaTabelaComponente = (props) => {

         const _Solicitacao = solicitacao ? solicitacao.find((item) => {
           return item.id === props.propsSolicitacao;
         })  : "Aguarde um instante, Solicitações carregando";

        const rotuloLab = laboratorios ? laboratorios.find((item) => {
          return item.id === props.propsLab;
        }) : "Aguarde um instante, Laboratórios carregando";

        const rotuloTurno = turno ? turno.find((item) => {
          return item.id === props.propsTurno;
        }) : "Carregando";

        const _alerta = props.propsAlerta === 'on' ? <AlertaIconeComponente/> : null
        
        
    
        return(
          
          <tr>            
          <td>{props.propsTarefas}{_alerta}</td>
          <td>{rotuloLab ? rotuloLab.abrev : "Carregando"}</td>
          <td>{_Solicitacao ? _Solicitacao.rotulo : "Carregando"}</td>
          <td>{props.propsDataSolic}</td>
          <td>{props.propsDataFinal}</td>
          <td>{rotuloTurno ? rotuloTurno.rotulo : "carregando"}</td>
        
          
          <td><img src='/img/remove1.png' className="botao-del" onClick={ () => { removerItem(props.idItem)}} /></td>              
        </tr>
          )
      }

      const incluirItem = () =>{
       
        if (novaSolicitacao && novoLaboratorio && novaTarefa){
          
          let idUltimoElemento = "0";

          if( tarefas.length > 0){
        const indiceUltimoElemento = tarefas.length -1;
        const ultimoElemento = tarefas [ indiceUltimoElemento];
         idUltimoElemento = ultimoElemento.id;
          }

        const idNovoItem = parseInt (idUltimoElemento) +1;
        
     

        const _data = moment(novaData).format('DD/MM/YYYY');
        const dataFinal = novoAlerta === 'on' ? moment(novaData).add(1, 'days').format('DD/MM/YYYY') 
       : moment(novaData).add(2, 'days').format('DD/MM/YYYY');

        const novoItem = {
          "id": idNovoItem,
          "tarefas": novaTarefa,
          "idLaboratorio": novoLaboratorio,
          "idSolicitacao": novaSolicitacao,
          "idTurno": novoTurno,
          "dataSolic": _data,
          "dataFinal": dataFinal,
          "urgente": novoAlerta,
        };
        setTarefas([...tarefas, novoItem]);

       } else{ alert("Preencha todos os campos!")}
        
      }

      

      const removerItem = (idExcluido) => {
        const _tarefas = tarefas.filter((item) => {
          return item.id !== idExcluido;
        });
        setTarefas(_tarefas); 
      }

      
    return (
        <>
        <div className="container">
          <div className="ladoEsquerdo">
            <div className="ladoEsquerdoTop">
              <div><h1>Solicitação de Serviços Técnicos</h1></div>
                <div>
                  <label htmlFor='campo-descricao'>Descrição: </label>
                  <input 
                  type="text" 
                  id='campo-descricao'
                  onChange={(event) => setNovaTarefa(event.target.value)}/>

                  </div>
                     
                <div>
                  <label htmlFor="Laboratorios">Laboratório: </label>
                  <select id='campo-laboratorio' value={novoLaboratorio} onChange={(event) => setNovoLaboratorio(event.target.value)}> 
                 
                  <option value={null}></option>
                    <OpcoesLaboratoriosComponente />
                  </select>
                  
                </div>
                <div>

                    <label htmlFor="solicitacao">Tipo de solicitação: </label>
                    <select value={novaSolicitacao} id='campo-solicitacao' onChange={(event) => setNovaSolicitacao(event.target.value)}>
                      <option value={null} ></option>
                      <OpcoesSolicitacaoComponente />
                    </select>
                </div>
                
                <div>
                  <label htmlFor="campo-date">Dia da Solicitaçao: </label>
                  <input type="date" id='campo-date'
                  onChange={(event) => setNovaData(event.target.value)}/>
                {/* <ReactCalendar/> */}
                </div>
                <div>
                  <p id='campo-turno'> Turno: </p>
                   <OpcoesTurnoComponente/>   
                 
                </div>
                <div className='campo-alerta'>
                  <p id='texto-alerta'>Urgente? </p>
                  <input 
                  type="checkbox" 
                  value='alerta' 
                  id='alert' 
                  style={{backgroundColor:'blue'}}
                  onChange={ () => setNovoAlerta(novoAlerta === 'on' ? 'off' : 'on')} /> 
                  <label htmlFor='alert'> Ligado</label>
                </div>
                <div>
                  <button id='solicitar' valor='solicitar' onClick={ () => incluirItem()}>Solicitar</button>
                </div>
              </div> 
              <div className="ladoEsquerdoBot">
                <h3>Legendas:</h3>
                {laboratorios.map(item => <p value={item.id} key={item.id}>{item.abrev} - {item.rotulo}</p>)}
              </div>
            </div>

          <div className="ladoDireito"> 
        <table> 
          <thead>
            <th id="titulo-soli" colSpan='7'>Solicitações</th>
            <tr>
              <th id="tarefas">Tarefas</th>
              <th>Laboratório</th>
              <th>Tipo de solicitação</th>
              <th>Dia da Solicitação</th>
              <th>Previsão de conclusão</th>
              <th>Turno</th>
              <th>Ação</th>
                
            </tr>
            
          </thead>
                
              <CorpoTabelaComponente />
              
            
          <tfoot>
            <tr>
              <td it='total-item' className='rodape' colSpan='7'> Total de itens:{tarefas.length}               
              </td>
              
            </tr>
          </tfoot>
          
        </table>
       
      </div>
      </div>

        </>


    )
}
